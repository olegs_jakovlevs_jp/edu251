/**
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskSecond
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Module registration file
 */

This module adds Slick Slider(https://kenwheeler.github.io/slick/) to your magento2 related product list.
Basically it adss slick JS file with default slick jquery configuration,
<file name.js> file with slider properties
and require-config.js file with required javascript files.