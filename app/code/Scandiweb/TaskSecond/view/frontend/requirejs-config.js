/*
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskSecond
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * JS require file
 */
var config = {
    map: {
        '*': {
            slider: 'Scandiweb_TaskSecond/js/slider',
            slick: 'Scandiweb_TaskSecond/js/vendor/slick.min',
        }
    }
};