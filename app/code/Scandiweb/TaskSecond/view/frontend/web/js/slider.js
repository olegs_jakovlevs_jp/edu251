/*
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskSecond
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Slider class configuration file
 */
require([
    'jquery',
    'slick'
], function ($) {
    jQuery(document).ready(function () {
        $('.slider').slick({
            dots: true,
            speed: 300,
            slidesToShow: 1,
            slidesToScroll: 1,
            mobileFirst: true,
            infinity: true,
            prevArrow: '<button class="slide-arrow prev-arrow"></button>',
            nextArrow: '<button class="slide-arrow next-arrow"></button>',
            responsive: [
                {
                    breakpoint: 1279,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 4,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 1023,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                }
            ]
        });
    });
});