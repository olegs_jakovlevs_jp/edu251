<?php
/**
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskThird
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Store configuration file
 */

namespace Scandiweb\TaskThird\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Store\Model\StoreFactory;
use Magento\Store\Model\ResourceModel\Store;
use Magento\Theme\Model\ResourceModel\Theme\CollectionFactory;
use Magento\Framework\App\Config\ConfigResource\ConfigInterface;

/**
 * Class NewMagentoStore
 * @package Rbj\CmsPage\Setup\Patch\Data
 */
class NewMagentoStore implements DataPatchInterface
{
    /**
     * @var StoreFactory
     */
    private $storeFactory;

    /**
     * @var Store
     */
    private $storeResourceModel;

    /**
     * @var CollectionFactory
     */
    private $themeCollectionFactory;

    /**
     * @var ConfigInterface
     */
    private $configInterface;

    /**\
     * @var array
     * Contains data for creating store
     */
    private $germanStoreData = [
        'name' => 'German store',
        'code' => 'store2_ger',
        'groupId' => 1,
        'websiteId' => 1,
        'sortOrder' => 10,
        'isActive' => true,
        'baseCurrency' => 'EUR',
        'defaultCurrency' => 'EUR',
        'allowedCurrency' => 'EUR,USD',
        'theme' => 'german',
    ];

    /**\
     * @var array
     * * Contains data for creating store
     */
    private $englishStoreData = [
        'name' => 'English store',
        'code' => 'store2_eng',
        'groupId' => 1,
        'websiteId' => 1,
        'sortOrder' => 10,
        'isActive' => true,
        'baseCurrency' => 'GBP',
        'defaultCurrency' => 'GBP',
        'allowedCurrency' => 'GBP,USD,EUR',
        'theme' => 'default',
    ];

    /**
     * @param StoreFactory $storeFactory
     * @param Store
     * @param CollectionFactory
     */
    public function __construct(
        StoreFactory $storeFactory,
        Store $storeResourceModel,
        CollectionFactory $themeCollectionFactory,
        ConfigInterface $configInterface

    ) {
        $this->storeFactory = $storeFactory;
        $this->storeResourceModel = $storeResourceModel;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->configInterface = $configInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->saveStore($this->englishStoreData);
        $this->saveStore($this->germanStoreData);
        $this->removeHtmlSuffix();
    }

    /**
     * @param $storeData
     * @return void
     * This function is used to create new magento store
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    public function saveStore($storeData)
    {
        $store = $this->storeFactory->create();
        $store->setName($storeData['name'])
            ->setCode($storeData['code'])
            ->setGroupId($storeData['groupId'])
            ->setWebsiteId($storeData['websiteId'])
            ->setSortOrder($storeData['sortOrder'])
            ->setIsActive($storeData['isActive']);
        $this->storeResourceModel->save($store);
        $this->addStoreConfig($store, $storeData['baseCurrency'],
            $storeData['defaultCurrency'],
            $storeData['allowedCurrency'],
            $storeData['theme']);
    }

    /**
     * @param $store
     * @param $currencyBase
     * @param $currencyDefault
     * @param $currencyAllowed
     * @param $themeName
     * @return void
     * This function set currency settings and theme to the created store
     */
    public function addStoreConfig($store, $currencyBase, $currencyDefault, $currencyAllowed, $themeName)
    {
        //Adding themes to the created stores
        $themeCollection = $this->themeCollectionFactory->create();
        $theme = $themeCollection->getThemeByFullPath('frontend/scandi/' . $themeName);
        $this->configInterface->saveConfig('design/theme/theme_id', $theme->getId(),
            'stores', $store->getId());

        //Allow currencies to the stores
        $this->configInterface->saveConfig('currency/options/allow ', $currencyAllowed,
            'stores', $store->getId());

        //Adding base currency to the stores
        $this->configInterface->saveConfig('currency/options/base', $currencyBase,
            'stores', $store->getId());

        //Adding default currency to the store
        $this->configInterface->saveConfig('currency/options/default', $currencyDefault,
            'stores', $store->getId());
    }

    /**
     * @return void
     * This function removes html suffix from product and category pages
     */
    public function removeHtmlSuffix()
    {
        //Removes .html suffix from category page
        $this->configInterface->saveConfig('catalog/seo/product_url_suffix', null,
            'default', '0');

        //Removes .html suffix from product page
        $this->configInterface->saveConfig('catalog/seo/category_url_suffix', null,
            'default', '0');
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}