/**
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskThird
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Module registration file
 */
This module contains 2 new store creating script with configurations:
Base, allow and default currency configuration.
Also this module:
Applies  themes to the stores
Removes .html suffix from product and category pages.\
(Scripts are changing magento2 core_config_data table data).


