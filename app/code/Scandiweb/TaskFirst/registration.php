<?php
/**
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskFirst
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Module registration file
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Scandiweb_TaskFirst',
    __DIR__
);