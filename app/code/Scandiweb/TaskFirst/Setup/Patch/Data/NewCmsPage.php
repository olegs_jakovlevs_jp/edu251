<?php
/**
 * @vendor    Scandiweb
 * @module    Scandiweb_TaskFirst
 * @author    Olegs Jakovlevs <olegs.jakovlevs.jp@scandiweb.com>
 * @copyright Copyright (c) 2019 Scandiweb, Inc (https://scandiweb.com)
 * @license   http://opensource.org/licenses/OSL-3.0 The Open Software License 3.0 (OSL-3.0)
 * Data patch file
 */

namespace Scandiweb\TaskFirst\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Cms\Model\PageFactory;

/**
 * Class NewCmsPage
 * @package Rbj\CmsPage\Setup\Patch\Data
 */
class NewCmsPage implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * @var PageFactory
     */
    private $pageFactory;

    /**
     * @var string
     */
    private $htmlCode = <<<EOF
     <div class="cms-page">
     <div class="row main-block">
        <div>
            <hr>
        </div>
        <div>
            <p>Most important information goes here. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec sit
                amet vestibulum ligula, eu ultricies nibh. Sed eu dolor neque. Vestibulum rhoncus ligula a leo placerat,
                vitae scelerisque purus malesuada. Pellentesque vel fringilla lacus. Mauris auctor lectus eget eros
                dapibus imperdiet. Maecenas at placerat odio.</p>
        </div>
    </div>
    <div class="row courier-delivery-block">
        <div>
            <h3>Courier Delivery Service</h3>
            <p>YSK.lv uses Purolator to deliver soft goods (ex. Bedding, Home Decor, Bath Products). It is important
                that you provide correct address information. If after three failed attempts of delivering your
                products, it will be held at your closest Purolator depot for two weeks for pick up. If your order is
                not picked up, it will be returned to the JYSK store that sent the product out. At this point, a refund
                will be given to you for only the products.</p>
            <p>You will receive a shipment email with a Purolator tracking number once your order is ready for delivery.
                You will be able to start tracking your order at <a>http://www.purolator.com</a> once Purolator picks up
                the products from the JYSK store.</p>
        </div>
        <div>&nbsp;<img class="paragraph-image" src="{{media url=&quot;Layer_75.png&quot;}}"></div>
    </div>
    <div class="local-delivery-block">
        <div>
            <h3>Local Delivery Service</h3>
            <p>YSK.lv uses 3rd party delivery companies to send furniture and mattress products. All deliveries will be
                a two man service and will be either first-point-of-entry or any room in the house depending on
                location.</p>
        </div>
        <div>
            <ul>
                <li>The best shipping company in the world</li>
                <li>Another bullet point outlining the good local deliveries</li>
                <li>The third bullet point example</li>
            </ul>
        </div>
    </div>
    <div class="row coupon-block">
        <div class="column coupon-element-vertical">
            <div class="row coupon-1">
                <div><img class="coupon-icon" src="{{media url=&quot;Email_icon_1.png&quot;}}"></div>
                <div>
                    <h3>Saņem atlaides kodu</h3>
                    <label>Saņem reģistrācijas apstiprinājumu un atlaides kodu uz e-pastu</label></div>
            </div>
            <div class="row coupon-2">
                <div><img class="coupon-icon" src="{{media url=&quot;Cart_Icon_1.png&quot;}}"></div>
                <div>
                    <h3>Saņem atlaides kodu</h3>
                    <label>Saņem reģistrācijas apstiprinājumu un atlaides kodu uz e-pastu</label></div>
            </div>
        </div>
        <div class="row coupon-element-horizontal">
            <div class="coupon-1"><img class="coupon-icon" src="{{media url=&quot;Email_icon_1.png&quot;}}">
                <h3>Saņem atlaides kodu</h3>
                <label>Saņem reģistrācijas apstiprinājumu un atlaides kodu uz e-pastu</label></div>
            <div class="coupon-2"><img class="coupon-icon" src="{{media url=&quot;Cart_Icon_1.png&quot;}}">
                <h3>Saņem atlaides kodu</h3>
                <label>Saņem reģistrācijas apstiprinājumu un atlaides kodu uz e-pastu</label></div>
        </div>
    </div>
    <div class="row half-column-block">
        <div>
            <h3>Half column content</h3>
            <p>JYSK.lv uses Purolator to deliver soft goods (ex. Bedding, Home Decor, Bath Products). It is important
                that you provide correct address information. If after three failed attempts of delivering your
                products, it will be held at your closest Purolator depot for two weeks for pick up. If your order is
                not picked up, it will be returned to the JYSK store that sent the product out. At this point, a refund
                will be given to you for only the products.</p>
            <p>You will receive a shipment email with a Purolator tracking number once your order is ready for delivery.
                You will be able to start tracking your order at <a>http://www.purolator.com</a> once Purolator picks up
                the products from the JYSK store.</p>
        </div>
        <div>
            <h3>Half column content</h3>
            <p>JYSK.lv uses Purolator to deliver soft goods (ex. Bedding, Home Decor, Bath Products). It is important
                that you provide correct address information. If after three failed attempts of delivering your
                products, it will be held at your closest Purolator depot for two weeks for pick up. If your order is
                not picked up, it will be returned to the JYSK store that sent the product out. At this point, a refund
                will be given to you for only the products.</p>
            <p>You will receive a shipment email with a Purolator tracking number once your order is ready for delivery.
                You will be able to start tracking your order at <a>http://www.purolator.com</a> once Purolator picks up
                the products from the JYSK store.</p>
        </div>
    </div>
    <div class="paragraph-block">
        <h3>Full width paragraph</h3>
        <h3 class="special-headline">H3 SUBTITLE</h3>
        <p>JYSK.lv uses 3rd party delivery companies to send furniture and mattress products. All deliveries will be a
            two man service and will be either first-point-of-entry or any room in the house depending on location.</p>
    </div>
    <div class="table-block">
        <table>
            <thead>
            <tr>
                <td>&nbsp;</td>
                <td>A TABLE EXAMPLE COLUMN 1</td>
                <td>ANOTHER COLUMN</td>
                <td>LAST COLUMN</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Row title<br> in two lnes</td>
                <td>Some text aligned in center of cell</td>
                <td>Second row example</td>
                <td>Last column row</td>
            </tr>
            <tr>
                <td>Small row title</td>
                <td>Lorem ipsum dolor jysk akmet.</td>
                <td>Consectuam</td>
                <td><span class="check">✓</span> Integrated</td>
            </tr>
            <tr>
                <td>Small row title</td>
                <td>Lorem ipsum dolor jysk akmet.</td>
                <td><span class="check">✓</span> Yes</td>
                <td>This was an empty cell</td>
            </tr>
            <tr>
                <td>Row title in three long lines to show fluidity</td>
                <td>Two lines content<br> can go there</td>
                <td>Numbers of row is unlimited, and the content will stretch according to it.</td>
                <td>Two lines content<br> can go there</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
EOF;

    /**
     * NewCmsPage constructor.
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param PageFactory $pageFactory
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        PageFactory $pageFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->pageFactory = $pageFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        /**
         * @array
         * Contains information what will be placed into cms page
         */
        $pageData = [
            'title' => 'Edu251',
            'page_layout' => '1column',
            'meta_keywords' => 'Page keywords',
            'meta_description' => 'Page description',
            'identifier' => 'edu251',
            'content_heading' => '',
            'content' => $this->htmlCode,
            'layout_update_xml' => '',
            'url_key' => 'edu251',
            'is_active' => 1,
            'store_id' => 0,
            'sort_order' => 0
        ];

        $this->moduleDataSetup->startSetup();
        /* Save CMS Page logic */
        $this->pageFactory->create()->setData($pageData)->save();
        $this->moduleDataSetup->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }
}